import csv
import uuid
import os
import pyfiglet
import pickle
import json

ORDERS_FILE_NAME = 'orders.csv'
INVENTORY_FILE_NAME = 'inventory.dat'
INVENTORY_TEMP_FILE_NAME = 'temp.dat'

INVAILD_INPUT = 'Invaild Input'

FIELDNAMES = ['id', 'name',  'phone_number', 'base', 'toppings']


def welcome():
    font = pyfiglet.figlet_format('Welcome to pizzajoint')
    print(font)


def init_orders():
    if not os.path.exists(ORDERS_FILE_NAME):

        with open(ORDERS_FILE_NAME, 'w') as file:
            writer = csv.DictWriter(file, fieldnames=FIELDNAMES)
            writer.writeheader()


def init_inventory():
    if not os.path.exists(INVENTORY_FILE_NAME):

        with open(INVENTORY_FILE_NAME, 'wb') as file:
            pickle.dump({
                'base': {
                    'thin': 45,
                    'thick': 45,
                    'crispy': 45
                },
                'toppings': {
                    'mushrooms': 100,
                    'peppers': 100,
                    'onions': 100,
                    'extra chesse': 100,
                    'tomatoes': 100
                }
            }, file)

# 1.Add Order


def add_order():
    order_id = uuid.uuid4()

    name = input("Enter your name: ")
    phone_number = int(input("Enter your phone number: "))
    base = input("Choose your base(thin | thick | crispy): ")

    print()

    toppings = []

    toppings_list = ['mushrooms', 'peppers',
                     'onions', 'extra chesse', 'tomatoes']

    while True:

        print(f'Your selected toppings are {toppings}')
        print()

        print('Available toppings are:-')
        for index in range(len(toppings_list)):

            topping = toppings_list[index]

            print(f'{index+1}.{topping}')

        print(f'{len(toppings_list)+1}.Exit')
        print()

        selected_topping = int(
            input(f"Enter your topping(1-{len(toppings_list)+1}): "))

        if selected_topping == len(toppings_list) + 1:
            break

        elif selected_topping in list(range(1, len(toppings_list)+1)):
            if toppings_list[selected_topping - 1] not in toppings:
                toppings.append(toppings_list[selected_topping - 1])

        else:
            print(INVAILD_INPUT)

    del topping

    print()

    order = {
        'id': str(order_id),
        'name': name,
        'phone_number': phone_number,
        'base': base,
        'toppings': toppings
    }

    print(json.dumps(order, indent=4))

    file_read = open(INVENTORY_FILE_NAME, 'rb')
    file_write = open(INVENTORY_TEMP_FILE_NAME, 'wb')

    inventory = pickle.load(file_read)

    inventory['base'][order['base']] -= 1

    for topping in order['toppings']:
        inventory['toppings'][topping] -= 1

    pickle.dump(inventory, file_write)

    file_read.close()
    file_write.close()

    os.remove(INVENTORY_FILE_NAME)
    os.rename(INVENTORY_TEMP_FILE_NAME, INVENTORY_FILE_NAME)

    with open(ORDERS_FILE_NAME, 'a') as file:

        writer = csv.DictWriter(file, fieldnames=FIELDNAMES)
        writer.writerow(order)


# 2.Show Orders


def show_orders():
    with open(ORDERS_FILE_NAME, 'r') as file:
        rows = csv.reader(file)

        for row in rows:
            print(row)

# 3.Delete Order


def delete_order():
    order_id = input("Enter order id: ")

    new_orders = []

    with open(ORDERS_FILE_NAME, 'r') as file:
        orders = csv.DictReader(file, fieldnames=FIELDNAMES)

        for order in orders:
            if order['id'] == order_id:
                print('deleted order', order['id'])
            else:
                new_orders.append(order)

    with open(ORDERS_FILE_NAME, 'w') as file:
        writer = csv.DictWriter(file, fieldnames=FIELDNAMES)

        writer.writerows(new_orders)


# 4.Update Order


def update_order():
    order_id = input("Enter order id: ")

    new_orders = []

    with open(ORDERS_FILE_NAME, 'r') as file:
        orders = csv.DictReader(file, fieldnames=FIELDNAMES)

        for order in orders:
            if order['id'] == order_id:

                while True:
                    print('Which field do you want to update ?')
                    print('1.Name')
                    print('2.Phone number')
                    print('3.Base')
                    print('4.Exit')

                    option = int(input("Enter your option(1-4): "))

                    if option == 1:
                        name = input('Enter updated name: ')

                        order['name'] = name

                        new_orders.append(order)

                    elif option == 2:
                        phone_number = input('Enter updated phone number: ')

                        order['phone_number'] = phone_number

                        new_orders.append(order)
                    elif option == 3:
                        base = input(
                            'Enter updated Base(thin | thick | crispy): ')

                        order['base'] = base

                        new_orders.append(order)
                    elif option == 4:
                        break
                    else:
                        print(INVAILD_INPUT)

            else:
                new_orders.append(order)

    with open(ORDERS_FILE_NAME, 'w') as file:
        writer = csv.DictWriter(file, fieldnames=FIELDNAMES)

        writer.writerows(new_orders)


# 5.Show Inventory


def show_inventory():
    with open(INVENTORY_FILE_NAME, 'rb') as file:
        inventory = pickle.load(file)
        print(json.dumps(inventory, indent=4))

# 6.Reload Inventory


def reload_inventory():

    with open(INVENTORY_FILE_NAME, 'wb') as file:
        pickle.dump({
            'base': {
                'thin': 45,
                'thick': 45,
                'crispy': 45
            },
            'toppings': {
                'mushrooms': 100,
                'peppers': 100,
                'onions': 100,
                'extra chesse': 100,
                'tomatoes': 100
            }
        }, file)

    print('Inventory Reloaded')


def main():
    welcome()

    init_orders()
    init_inventory()

    while True:
        print()
        print('====== Main Menu ======')
        print()
        print('1.Add order')
        print('2.Show orders')
        print('3.Delete order')
        print('4.Update order')
        print('5.Show inventory')
        print('6.Reload inventory')
        print('7.Exit')
        print()

        option = int(input("Enter your option(1-7): "))

        print()

        if option == 1:
            add_order()

        elif option == 2:
            show_orders()

        elif option == 3:
            delete_order()

        elif option == 4:
            update_order()

        elif option == 5:
            show_inventory()

        elif option == 6:
            reload_inventory()

        elif option == 7:
            print('Bye , have a Nice day')
            break

        else:
            print(INVAILD_INPUT)

        print()


if __name__ == "__main__":
    main()
